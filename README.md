# Tiresize
Program and library for calculating tire sizes.

## Usage
The main way to use the crate as a library is through the `TireSize` struct.

```rust
use tiresize::TireSize;

fn main() {
    let ts = TireSize::parse("200/75R15").unwrap();
    println!("width {0}", ts.width());
    println!("height {0:.2}", ts.height()); // Specify the precision
}
```

The width and height return a `Unit` that can be converted between inches,
centimeters, and millimeters.

The command line program expects a tiresize string. Optionally it accepts an
option to change the display units.

```text
Calculate tire size

Usage: tiresize [OPTIONS] <tire sizes>...

Arguments:
  <tire sizes>...  The tire size standard notation (example P175/65R15)

Options:
  -u, --unit <unit>  The unit to display the measurement in [default: inch] [possible values: inch, in, centimeter, cm, millimeter, mm]
  -h, --help         Print help information
  -V, --version      Print version information
```
