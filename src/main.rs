use clap::{Arg, ArgAction, Command};
use tiresize::TireSize;

const ARG_UNIT_ID: &'static str = "unit";
const ARG_TIRE_SIZES_ID: &'static str = "tire sizes";

fn main() {
    let matches = Command::new("tiresize")
        .version(env!("CARGO_PKG_VERSION"))
        .author(env!("CARGO_PKG_AUTHORS"))
        .about("Calculate tire size")
        .arg(
            Arg::new(ARG_UNIT_ID)
                .action(ArgAction::Set)
                .help("The unit to display the measurement in")
                .long("unit")
                .short('u')
                .default_value("inch")
                .value_parser(["inch", "in", "centimeter", "cm", "millimeter", "mm"]),
        )
        .arg(
            Arg::new(ARG_TIRE_SIZES_ID)
                .action(ArgAction::Append)
                .index(1)
                .required(true)
                .help("The tire size standard notation (example P175/65R15)"),
        )
        .get_matches();

    let unit = matches.get_one::<String>(ARG_UNIT_ID).unwrap();

    let mut tire_sizes = matches
        .get_many::<String>(ARG_TIRE_SIZES_ID)
        .expect("tire sizes");

    if tire_sizes.len() == 1 {
        let ts = TireSize::parse(tire_sizes.next().unwrap()).expect("failed to parse tire size");

        let (width, height) = match unit.as_str() {
            "inch" | "in" => (ts.width().to_inch(), ts.height().to_inch()),
            "centimeter" | "cm" => (ts.width().to_centimeters(), ts.height().to_centimeters()),
            "millimeter" | "mm" => (ts.width().to_millimeter(), ts.height().to_millimeter()),
            _ => unimplemented!(),
        };

        println!("Width: {width:.2} Height: {height:2.}");
    } else {
        for tire_size in tire_sizes {
            let ts = TireSize::parse(tire_size).expect("failed to parse tire size");

            let (width, height) = match unit.as_str() {
                "inch" | "in" => (ts.width().to_inch(), ts.height().to_inch()),
                "centimeter" | "cm" => (ts.width().to_centimeters(), ts.height().to_centimeters()),
                "millimeter" | "mm" => (ts.width().to_millimeter(), ts.height().to_millimeter()),
                _ => unimplemented!(),
            };

            println!("{tire_size} Width: {width:.2} Height: {height:.2}");
        }
    }
}
