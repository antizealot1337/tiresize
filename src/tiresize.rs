use crate::unit::Unit;
/// Represents a tire size.
pub struct TireSize {
    width: Unit,
    aspect: f64,
    rim: Unit,
}

impl TireSize {
    /// Creates a new TireSize.
    fn new(width: Unit, aspect: f64, rim: Unit) -> Self {
        Self { width, aspect, rim }
    }

    /// Parses a TireSize from the provided string. It expects the tire size to
    /// be in the form of <WIDTH>/<ASPECT>R<RIM> where with is the width of the
    /// tire in millimeters, aspect is the height of the side was represented as
    /// the precent of the width, and the rim size in inches. The strings may
    /// begin with "P", "p", "LT", or "lt" and still be valid. The "R" in the
    /// string need not be "R" specifically.
    pub fn parse(s: &str) -> Result<Self, Box<dyn std::error::Error>> {
        let start: usize = match s {
            s if s.starts_with('P') || s.starts_with('p') => 1,
            s if s.starts_with("LT") || s.starts_with("lt") => 2,
            _ => 0,
        };

        let slash = s.find('/').ok_or(format!("invalid tire size {0}", s))?;
        let r = s.len() - 2;

        let width: f64 = s.get(start..slash).unwrap().parse()?;
        let aspect: f64 = s.get(slash + 1..r - 1).unwrap().parse()?;
        let rim: f64 = s.get(r..).unwrap().parse()?;

        Ok(TireSize::new(
            Unit::Millimeter(width),
            aspect,
            Unit::Inch(rim),
        ))
    }

    /// Returns the width of the TireSize.
    pub fn width(&self) -> Unit {
        self.width
    }

    /// Returns the height of the TireSize.
    pub fn height(&self) -> Unit {
        Unit::Millimeter(self.width.value() * self.aspect * 0.02 + self.rim.to_millimeter().value())
    }
}

#[cfg(test)]
mod test {
    use super::*;

    const WIDTH: f64 = 100.0;
    const ASPECT: f64 = 100.0;
    const RIM: f64 = 15.0;

    #[test]
    fn test_parse_tire_size() {
        let ts = TireSize::parse(format!("{0}/{1}R{2}", WIDTH, ASPECT, RIM).as_str()).unwrap();

        assert_eq!(ts.width, Unit::Millimeter(WIDTH));
        assert_eq!(ts.aspect, ASPECT);
        assert_eq!(ts.rim, Unit::Inch(RIM));
    }

    #[test]
    fn test_tire_size_width() {
        let ts = TireSize::parse(format!("{0}/{1}R{2}", WIDTH, ASPECT, RIM).as_str()).unwrap();

        assert_eq!(Unit::Millimeter(WIDTH), ts.width());
    }

    #[test]
    fn test_tire_size_height() {
        let ts = TireSize::parse(format!("{0}/{1}R{2}", WIDTH, ASPECT, RIM).as_str()).unwrap();

        assert_eq!(
            Unit::Millimeter(WIDTH * ASPECT * 0.02 + 15.0 * 25.4),
            ts.height()
        );
    }
}
