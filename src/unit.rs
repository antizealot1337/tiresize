use std::fmt::Display;

#[derive(PartialEq, Debug, Copy, Clone)]
/// A Unit of measurement.
pub enum Unit {
    Inch(f64),
    Centimeter(f64),
    Millimeter(f64),
}

impl Unit {
    /// Returns the f64 values of the Unit.
    pub(crate) fn value(&self) -> f64 {
        match self {
            Unit::Inch(i) => *i,
            Unit::Centimeter(i) => *i,
            Unit::Millimeter(i) => *i,
        }
    }

    /// Returns this Unit's value as a new Unit::Centimeter.
    pub fn to_centimeters(self) -> Self {
        let val = match self {
            Unit::Inch(v) => v * 2.54,
            Unit::Centimeter(v) => v,
            Unit::Millimeter(v) => v / 10.0,
        };

        Unit::Centimeter(val)
    }

    /// Returns this Unit's value as a new Unit::Millimeter.
    pub fn to_millimeter(self) -> Self {
        let val = match self {
            Unit::Inch(v) => v * 25.4,
            Unit::Centimeter(v) => v * 10.0,
            Unit::Millimeter(v) => v,
        };

        Unit::Millimeter(val)
    }

    // Returns this Unit's value converted to a Unit::Inch.
    pub fn to_inch(self) -> Self {
        let val = match self {
            Unit::Inch(v) => v,
            Unit::Centimeter(v) => v / 2.54,
            Unit::Millimeter(v) => v / 25.4,
        };

        Unit::Inch(val)
    }
}

impl Display for Unit {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let (amount, unit) = match self {
            Unit::Inch(value) => (*value, "in"),
            Unit::Centimeter(value) => (*value, "cm"),
            Unit::Millimeter(value) => (*value, "mm"),
        };
        if let Some(precision) = f.precision() {
            write!(f, "{amount:.*} {unit}.", precision)
        } else {
            write!(f, "{amount} {unit}.")
        }
    }
}

#[cfg(test)]
mod test {
    use super::Unit::*;

    #[test]
    fn test_inch_to_inch() {
        assert_eq!(Inch(1.0).to_inch(), Inch(1.0));
    }

    #[test]
    fn test_inch_to_centimeter() {
        assert_eq!(Inch(1.0).to_centimeters(), Centimeter(2.54));
    }

    #[test]
    fn test_inch_to_millimeter() {
        assert_eq!(Inch(1.0).to_millimeter(), Millimeter(25.4));
    }

    #[test]
    fn test_centimeter_to_inch() {
        assert_eq!(Centimeter(2.54).to_inch(), Inch(1.0));
    }

    #[test]
    fn test_centimeter_to_centimeter() {
        assert_eq!(Centimeter(100.0).to_centimeters(), Centimeter(100.0));
    }

    #[test]
    fn test_centimeter_to_millimeter() {
        assert_eq!(Centimeter(1.0).to_millimeter(), Millimeter(10.0));
    }

    #[test]
    fn test_millimeter_to_inch() {
        assert_eq!(Millimeter(25.4).to_inch(), Inch(1.0));
    }

    #[test]
    fn test_millimeter_to_centimeter() {
        assert_eq!(Millimeter(10.0).to_centimeters(), Centimeter(1.0));
    }

    #[test]
    fn test_millimeter_to_millimeter() {
        assert_eq!(Millimeter(1.0), Millimeter(1.0));
    }

    #[test]
    fn test_inch_fmt() {
        assert_eq!(format!("{0}", Inch(1.0)), "1 in.");
    }

    #[test]
    fn test_inch_fmt_precision() {
        assert_eq!(format!("{0:.2}", Inch(1.0)), "1.00 in.");
    }

    #[test]
    fn test_centimeter_fmt() {
        assert_eq!(format!("{0}", Centimeter(1.0)), "1 cm.");
    }

    #[test]
    fn test_centimeter_fmt_precision() {
        assert_eq!(format!("{0:.2}", Centimeter(1.0)), "1.00 cm.");
    }

    #[test]
    fn test_millimeter_fmt() {
        assert_eq!(format!("{0}", Millimeter(1.0)), "1 mm.");
    }

    #[test]
    fn test_millimeter_fmt_precision() {
        assert_eq!(format!("{0:.2}", Millimeter(1.0)), "1.00 mm.");
    }
}
